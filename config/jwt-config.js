module.exports = {
    secret: "thisIsSecretKey",
    expiresIn: 120, //for 2 min // exp: Math.floor(Date.now() / 1000) + (60 * 60), //Signing a token with 1 hour of expiration
    notBefore: 2, //after 2 sec will be able to use token value
    audience: "site-users",
    issuer: "this is issuer",
    algorithm: "HS256" // HS384 //HS256 /
};