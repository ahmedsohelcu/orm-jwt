const express = require('express');
const JWT = require('jsonwebtoken');
const jwtConfig = require('./config/jwt-config');
const Sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const bodyParser = require('body-parser');

//our custom middleware
const JwtMiddleware = require('./config/jwt.middleware');

const app = express();

//======================================================================
//bodyParser
//======================================================================
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));


//ROUTE
//====================================================================
app.get("/", (req, res) => {
    res.status(200).json({
        status: 1,
        message: "welcome to home page.."
    });
});



//======================================================================
//db connection
//======================================================================
const sequelize = new Sequelize("orm_jwt", "root", "", {
    host: "localhost",
    dialect: "mysql"
});
sequelize.authenticate()
    .then((data) => {
        console.log("Database is successfully connected")
    })
    .catch(error => {
        console.log(error);
    })


//======================================================================
//define  Model
//======================================================================
var User = sequelize.define('User', {
    id: {
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER
    },
    name: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    email: {
        type: Sequelize.STRING(50),
        allowNull: false
    },
    password: {
        type: Sequelize.STRING(150),
        allowNull: false
    },
    status: {
        type: Sequelize.INTEGER,
        defaultValue: 1
    },
}, {
    timestamps: false,
    ModelName: "User"
});

User.sync();

//======================================================================
//User registration
//======================================================================
app.post('/user-register', (req, res) => {
    // const { name, email, password, status } = req.body;

    let name = req.body.name;
    let email = req.body.email;
    let password = bcrypt.hashSync(req.body.password, 10);
    let status = req.body.status;

    User.findOne({
            where: {
                email: email
            }
        })
        .then(data => {
            if (data) {
                res.status(200).json({
                    status: 0,
                    message: "user already exist with this email"
                })
            } else {
                User.create({
                        name,
                        email,
                        password,
                        status
                    })
                    .then(data => {
                        res.status(200).json({
                            status: "1",
                            message: "user register successfully.."
                        })
                    })
                    .catch(error => {
                        res.status(404).json({
                            status: "0",
                            message: "failed to register user.." + error
                        })
                    });

            }
        })
        .catch(error => {
            res.status(404).json({
                status: "0",
                message: "some thing wrong" + error
            })
        });
});

//======================================================================
//validate token api
//======================================================================
app.post('/token-validate', (req, res) => {
    //console.log(req.headers); //
    let userToken = req.headers["authorization"];

    if (userToken) {
        //we have token
        JWT.verify(userToken, jwtConfig.secret, (error, decoded) => {
            if (error) {
                res.status(500).json({
                    status: 0,
                    message: "Invalid Token!",
                    data: error
                });
            } else {
                res.status(200).json({
                    status: 1,
                    message: "Token is valid",
                    data: decoded
                });
            }
        });
    }

});

//======================================================================
//Login user
//======================================================================
app.post('/login', (req, res) => {

    User.findOne({
            where: {
                email: req.body.email
            }
        })
        .then(user => {
            if (user) {
                //user found with this email
                if (bcrypt.compareSync(req.body.password, user.password)) {
                    //generate token..
                    //-------------------------------------------
                    let userToken = JWT.sign({
                        id: user.id,
                        email: user.email
                    }, jwtConfig.secret, {
                        expiresIn: jwtConfig.expiresIn,
                        notBefore: jwtConfig.notBefore,
                        audience: jwtConfig.audience,
                        issuer: jwtConfig.issuer,
                        algorithm: jwtConfig.algorithm
                    });

                    //password matched          
                    //-------------------------------------------
                    res.status(200).json({
                        status: 1,
                        message: "User Logged In Successfully",
                        toke: userToken
                    });
                } else {
                    res.status(500).json({
                        status: 0,
                        message: "Email or Password not match !"
                    });
                }

            } else {
                //user not found with this email
                res.status(404).json({
                    status: 0,
                    message: "User not exist with this email"
                });
            }
        })
        .catch(error => {
            res.status(404).json({
                status: 0,
                message: "some thing wrong" + error
            })
        });
});

//======================================================================
//uses of middleware..
//======================================================================
app.post('/profile', JwtMiddleware.checkToken, (req, res) => {
    res.status(200).json({
        status: 1,
        userdata: req.user,
        message: "Token value parsed"
    });
});

/*
app.delete('/user-delete', (req, res) => {
    User.destroy({
        where: {},
        truncate: true
    })
});
*/

//======================================================================
//port
//======================================================================
const PORT = 3000
    //======================================================================


//======================================================================
//listen request here...
//======================================================================
app.listen(PORT, () => {
    console.log("Application is running with port " + PORT);
}); //======================================================================